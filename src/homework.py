def multiply_by_three(number_list):
    return [number * 3 for number in number_list]


def generate_email(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


if __name__ == '__main__':
    # Task 3
    numbers = [4, 5, 8]
    result = multiply_by_three(numbers)
    print(result)

    # Task 2
    janusz_email = generate_email("Janusz", "Nowak")
    barbara_email = generate_email("Barbara", "Kowalska")
    print(janusz_email)
    print(barbara_email)

    # Task 1
    emails = ["a@example.com", "b@example.com"]
    number_of_elements = len(emails)

    print(number_of_elements)
    print(emails[0])
    print(emails[-1])
    emails.append("c@exemple.com")
    print(emails)
