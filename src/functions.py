def square_give_number(imput_number):
    return imput_number ** 2

def volume_of_cuboid(a, b , c):
    return  a * b * c

def convert_celcius_to_fahrenheit(temp_in_celcius):
    return 9 / 5 * temp_in_celcius + 32

if __name__ == '__main__':
    # Exercise 1
    zero_square = square_give_number(0)
    sixteen_square = square_give_number(16)
    float_square = square_give_number(2.55)
    print("Square of 0 is", zero_square)
    print("Square of 16 is", sixteen_square)
    print("Square of 2.55 is", float_square)

    # Exercise 2
    volume_result = volume_of_cuboid(3, 5,7)
    print(volume_result)

    # Exercise 3
    fahrenheit_volume = convert_celcius_to_fahrenheit(20)
    print("To convert Celsius to Fahrenheit, C to F formula which is °F = (9/5) °C+32.", fahrenheit_volume)