from faker import Faker
import random
fake = Faker()


def generate_random_employee_dictionary():
    return {
        "email": fake.ascii_safe_email(),
        "seniority_years": random.randint(1, 40),
        "female": random.choice([True, False]),
    }

def generate_array_of_employee_dictionaries(number_of_dictionaries):
    return [generate_random_employee_dictionary() for i in range(number_of_dictionaries)]

def get_emails_of_employees_with_seniority_years_greater_then_10(list_of_employees):
    output_emails = []

        for employees_dictionaries


if __name__ == '__main__':
    for n in range(5):
        print(generate_random_employee_dictionary())

print(generate_array_of_employee_dictionaries(20))