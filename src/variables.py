friend_first_name = "Adam"
friend_age = 36
friend_number_of_pets = 0
friend_has_driving_license = True
friend_length_in_years = 20.5

print("My friend's name:", friend_first_name)
print("My friend's age:", friend_age)
print("My friend's number of pets:", friend_number_of_pets)
print("My friend's has driving license:", friend_has_driving_license)
print("How many years we've been friends:", friend_length_in_years)
