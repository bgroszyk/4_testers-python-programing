def city_greeter (name, city):
    greeter = f"Witaj {name.upper()}! Miło Cię widzieć w naszym mieście: {city.upper()}!"
    print(greeter)

if __name__ == '__main__':
    first_name = "Bartek"
    last_name = "Groszyk"
    email = first_name.lower() + "." + last_name.lower() + "@4testers"
    email_formater = f"{first_name.lower()}.{last_name.lower()}@4testers"

    print(email)
    print(email_formater)

    city_greeter("Michał", "Toruń")
    city_greeter("Beata", "Warszawa")
